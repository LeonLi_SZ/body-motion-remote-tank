def on_button_pressed_a():
    if evt8000_free:
        extra_btn_event(8001)
input.on_button_pressed(Button.A, on_button_pressed_a)

def on_button_pressed_ab():
    if evt8000_free:
        extra_btn_event(8002)
input.on_button_pressed(Button.AB, on_button_pressed_ab)

def on_button_pressed_b():
    if evt8000_free:
        extra_btn_event(8003)
input.on_button_pressed(Button.B, on_button_pressed_b)

def extra_btn_event(value: number):
    global evt8000_free
    evt8000_free = False
    radio.raise_event(8000, value)
    serial.write_line("button event " + convert_to_text(value))
    basic.pause(200)
    evt8000_free = True

def on_logo_touched():
    if evt8000_free:
        extra_btn_event(8004)
input.on_logo_event(TouchButtonEvent.TOUCHED, on_logo_touched)

stop_tmo = 0
stop_debounce = 0
accx = 0
accz = 0
evt8000_free = False
input.set_accelerometer_range(AcceleratorRange.ONE_G)
radio.set_group(180)
evt8000_free = True
basic.show_string("Tx")
serial.write_line("*************************************************************")
serial.write_line("* Freda Li's Body Motion controller V3.3                    *")
serial.write_line("* Nov 14, 2022. 11:15                                       *")
serial.write_line("* moving data accX & accZ sent via RF every 20ms            *")
serial.write_line("* and sent via COM port every 2 seconds for debug reference *")
serial.write_line("*************************************************************")

def on_every_interval():
    global accz, accx
    accz = input.acceleration(Dimension.Z)
    accx = input.acceleration(Dimension.X)
loops.every_interval(20, on_every_interval)

def on_every_interval2():
    global stop_debounce, stop_tmo
    if abs(accx) >= 200 or abs(accz) >= 200:
        radio.send_value("accz", accz)
        radio.send_value("accx", accx)
        # stop_debounce: 10 for 10 * 20ms = 200ms
        stop_debounce = 10
        # stop_tmo: 15 for 15 * 100ms = 1500ms = 1.5 sec
        stop_tmo = 15
    elif stop_debounce > 0:
        stop_debounce += -1
        if stop_debounce <= 0:
            radio.raise_event(9000, 9002)
            stop_debounce = 0
            serial.write_line("STOP by debounce")
loops.every_interval(20, on_every_interval2)

def on_every_interval3():
    serial.write_line("z " + convert_to_text(accz) + ", x  " + convert_to_text(accx))
loops.every_interval(2000, on_every_interval3)

def on_every_interval4():
    global stop_tmo
    if stop_tmo > 0:
        stop_tmo += -1
        if stop_tmo <= 0:
            radio.raise_event(9000, 9001)
            stop_tmo = 0
            serial.write_line("STOP by timeout")
loops.every_interval(100, on_every_interval4)

def on_every_interval5():
    led.stop_animation()
    led.plot(int(pins.map(accx, -1023, 1023, 0, 5)),
        int(pins.map(accz, -1023, 1023, 0, 5)))
loops.every_interval(100, on_every_interval5)

def on_every_interval6():
    led.set_brightness(pins.map(input.light_level(), 0, 255, 50, 200))
loops.every_interval(5000, on_every_interval6)
